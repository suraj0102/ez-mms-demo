const express = require("express");
const router = express.Router();
const axios = require("axios").default;

// Generate Ez Texting API access token
async function getAccessToken() {
  try {
    const postData = {
      appKey: "",
      appSecret: "",
    };
    const TOKEN_API = "https://a.eztexting.com/v1/tokens/create";
    const response = await axios.post(`${TOKEN_API}`, postData);
    return response.data.accessToken;
  } catch (error) {
    console.error(error);
    return error;
  }
}

// Send message using Ez Texting API access token
async function sendMessage(accessToken, messageData) {
  try {
    const EZ_TEXTING_API = "https://a.eztexting.com/v1/messages";
    const response = await axios.post(`${EZ_TEXTING_API}`, messageData, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
        Accept: "*/*",
      },
      params: {
        format: "json",
      },
    });
    return response.data;
  } catch (error) {
    console.error(error);
    return error;
  }
}

router.get("/", async function (req, res) {
  const accessToken = await getAccessToken();
  console.log(accessToken);
  const messageData = {
    message: "Example test Message",
    // fromNumber: "3852780587",
    companyName: "codeplateau",
    mediaUrl: "https://eztxt.s3.amazonaws.com/295127/mms/gurr_1679519642.jpg",
    // strictValidation: false,
    toNumbers: ["8015553890"], // Replace with actual phone number(s)
  };
  const response = await sendMessage(accessToken, messageData);
  console.log(response);
  res.send(response);
});

module.exports = router;
