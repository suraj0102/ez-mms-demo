const express = require("express");
const cors = require("cors");

const app = express();

//Enable cors policy
app.use(cors());

//regular middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//import all routes here
const indexRouter = require("./routes/index");

//router middleware
app.use("/", indexRouter);

//export app js
module.exports = app;
