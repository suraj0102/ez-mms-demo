const app = require("./app");
// const connectWithDb = require("./config/db");
// require("dotenv").config();

// connect with databases
// connectWithDb();

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server is running at port: ${port}`);
});
